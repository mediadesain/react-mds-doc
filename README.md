# Instalasi ReactJs

Project dokumentasi playground ini dibuat oleh [Mediadesain](https://www.mediadesain.com).

## Install NodeJs

Pastikan Install NodeJS terlebih Dahulu

## Install ReactJs

Install ReactJs dengan perintah CMD/Terminal

### `npm install -g create-react-app`
Jalankan perintah diatas untuk menginstal reactjs.

### `npx create-react-app mediadesain-web`
Jalankan perintah diatas untuk membuat project baru reactjs

### `npm start`
Untuk menjalankan server localhost [http://localhost:3000](http://localhost:3000).


### `npm run build`
Jalankan perintah diatas untuk build production untuk dihosting folder output ada difolder `build`

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can�t go back!**


#### Usefull Dependencies:
* ReactJs Page Routing Plugin `npm i react-router-dom`
* ReactJs Pagination Plugin `npm i react-js-pagination`
* ReactJs Slider Plugin `npm i react-slick`
* Hosting & database Firebase `npm i firebase`
* react-hook-form `npm i react-hook-form`
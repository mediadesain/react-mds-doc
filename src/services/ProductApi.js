import React from 'react';
import firebase from '../firebase';

function usePosts(props) {
  const [data, setData] = React.useState({});

  React.useEffect( () => {
    firebase.database().ref('/products').on('value', 
    (snapshot) => { setData({sourcedata: Object.values(snapshot.val())}) },
    (error) => { setData({sourcedata:[]}); }
  );
  }, [setData]);

  return data;
}

export default usePosts;

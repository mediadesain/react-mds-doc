/*=====================================================
---------------- Contoh Parsing Data ----------------
=====================================================*/

import React, { Component } from "react";

class ParsingData extends Component {
  constructor(props) {

    super(props); //Location Paramnya console.log(props)
    this.state = {
      obj: { brand: "Ford", description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquam scelerisque cursus. Fusce iaculis ornare consectetur. Sed consectetur nisi eget eleifend vehicula.", model: "Mustang", color: "Red", year: 1964, date: 1613026223952, money: 50000 },
      arr_simple: ["Kucing", "Anjing", "Kelinci", "Burung", "Ayam"],
      arr_child: [
        { id: 5151, nama: "Roy", kelamin: "Pria", kota: "Jakarta", url: "http://www.roy.com", hobi: ["Game"] },
        { id: 5152, nama: "Jhon", kelamin: "Pria", kota: "Jakarta", url: "http://www.jhon.com", hobi: ["Game", "Sepak Bola"] },
        { id: 5153, nama: "James", kelamin: "Pria", kota: "Bandung", url: "http://www.james.com", hobi: ["Game", "Sepak Bola", "Traveling"] },
        { id: 5154, nama: "Jane", kelamin: "Wanita", kota: "Bandung", url: "http://www.jane.com", hobi: ["Traveling"] },
        { id: 5155, nama: "Rose", kelamin: "Wanita", kota: "Bandung", url: "http://www.rose.com", hobi: ["Game", "Traveling"] },
        { id: 5156, nama: "Alice", kelamin: "Wanita", kota: "Surabaya", url: "http://www.alice.com", hobi: ["Sepak Bola", "Traveling"] }
      ]
    };

    //Render List Item On Function
    this.arrSimpleRender = this.state.arr_simple.map((value, index) => {
      return (
        <li key={index}>{value} <small>(Data ke {index})</small></li>
      );
    })
    this.arrChildRender = this.state.arr_child.map((value, index) => {
      return (
        <li key={index}>
          {value.nama} - {value.kelamin}<br />
          {value.kota}<br />
          {value.hobi + ","}
        </li>
      );
    })

  }
  
  componentDidMount() {
    document.title = "Parsing Data";
  }

  render() {
    return (
      <div>
        <h3>Demo Parsing Data</h3>
        <p>{this.state.obj.brand} - {this.state.obj.model} - {this.state.obj.color} - {this.state.obj.year} - {this.state.obj.date} - {this.state.obj.money}</p>
        <p>{this.state.obj.description}</p>
        <br/>

        {/*--Sample Data--*/}
        <h4>Contoh Data JSON</h4>
        <div className="row">
          <div className="column" style={{minWidth:'60%'}}>
          <pre>data: {JSON.stringify(this.state.arr_simple,0,2)}</pre>
          </div>
          <div className="column">
          <pre>data: {JSON.stringify(this.state.arr_child.slice(0, 3),0,1)}<br/>...</pre>
          </div>
        </div>
        
        {/*--Render dengen fungsi binding--*/}
        <h4>Render Dalam Function</h4>
        <div className="row">
          <div className="column" style={{minWidth:'60%'}}>
            <p>Simple Repeat Item (Render Function)</p>
            <ul>{this.arrSimpleRender}</ul>
          </div>
          <div className="column">
            <p>Repeat Item with Child</p>
            <ul>{this.arrChildRender}</ul>
          </div>
        </div>

        {/*--Direct Render--*/}
        <h4>Direct Render dalam Tag</h4>
        <div className="row">
          <div className="column" style={{minWidth:'60%'}}>
            <p>Simple Repeat Item</p>
            <ul>
              {this.state.arr_simple.map( (value, index) => {
                return (
                  <li key={index}>{value} <small>(Data ke {index})</small></li>
                );
              } )}
            </ul>
          </div>
          <div className="column">
            <p>Repeat Item with Child</p>
            <ul>
              {this.state.arr_child.map( (value, index) => {
                return (
                  <li key={index}>
                    {value.nama} - {value.kelamin}<br />
                    {value.kota}<br />
                    {value.hobi + ","}
                  </li>
                );
              } )}
            </ul>
          </div>
        </div>
        

      </div>
    );
  }
}

export default ParsingData;

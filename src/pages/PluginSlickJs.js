/*=====================================================
---------------- Contoh Form Submition ----------------
=====================================================*/

import React, { useState, useEffect } from 'react';
import { useJudul } from '../helper';
import Slider from "react-slick";

const SlickJs = (props) => {
  useJudul("Slick Slider");
  var settings = {
    arrows: true,
    autoplay: false,
    autoplaySpeed: 2000,
    centerMode: false,
    centerPadding: '40px',
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    variableWidth: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  };

  useEffect( () => {
  }, []);

  return (
    <>
      <div className="row">
        <div className="column" style={{minWidth:'60%'}}>
        <h3>Slider Plugin <small>(<i>react-slick</i>)</small></h3>
        <Slider {...settings}>
          <div className="slick-content"><h3>1</h3></div>
          <div className="slick-content"><h3>2</h3></div>
          <div className="slick-content"><h3>3</h3></div>
          <div className="slick-content"><h3>4</h3></div>
          <div className="slick-content"><h3>5</h3></div>
          <div className="slick-content"><h3>6</h3></div>
        </Slider>
        <br/>
        <small><i>Doc: https://github.com/akiran/react-slick</i></small>
        </div>
        <div className="column">
          <h4>Setting</h4>
          <pre>{JSON.stringify(settings,0,2)}</pre>
        </div>
      </div>
    </>
  )
}

export default SlickJs;
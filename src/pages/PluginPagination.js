
/*===============================================================================================
------------------- Contoh Parsing Data dengan Pagination (react-js-pagination)-------------------
================================================================================================*/

import React, { useEffect, useState } from "react";
import { useJudul } from '../helper';
import Pagination from "react-js-pagination";

const PluginPagination = (props) => {
   useJudul('Plugin Pagination');
   const query = new URLSearchParams(props.location.search).get("page");
   // Data to be rendered using pagination.
   const todos = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
   const todosPerPage = 3;
   const [ activePage, setCurrentPage ] = useState(1);

   // Logic for displaying pagination
   const indexOfLastTodo  = activePage * todosPerPage;
   const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
   const currentTodos     = todos.slice( indexOfFirstTodo, indexOfLastTodo );

   useEffect( () => {
      if(!props.location.search){ props.history.push('pagination?page=1') }
      else setCurrentPage(parseInt(query))
   }, [] )

   const handlePageChange = ( pageNumber ) => {
      setCurrentPage( pageNumber )
      props.history.push('pagination?page='+pageNumber)
   };

   return (
      <div>
         <h3>Pagination Plugin <small>(<i>react-js-pagination</i>)</small></h3>
        <ul>
            {currentTodos.map( (todo, index) => (
                <li key={ index }>{ todo }</li>
            ) )}
        </ul>
        <Pagination
            firstPageText={"«"}
            prevPageText={"‹"}
            nextPageText={"›"}
            lastPageText={"»"}
            activePage={ activePage }
            itemsCountPerPage={ 3 }
            totalItemsCount={ todos.length }
            pageRangeDisplayed={ 3 }
            onChange={ handlePageChange }
        />
        <br/>
        <small><i>Doc: https://www.npmjs.com/package/react-js-pagination</i></small>
      </div>
   )

}

export default PluginPagination;
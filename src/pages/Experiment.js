/*=====================================================
---------------- Contoh Form Submition ----------------
=====================================================*/

import React, { useState, useEffect } from 'react';
import {useJudul, usePerbaharuiForm} from '../helper';

const Experiment = (props) => {
  useJudul("Test");
  /*-----------------Experiment----------------------------------Experiment End-----------------*/
  const name = usePerbaharuiForm('Deler Kami');
  const cars = usePerbaharuiForm('suzuki');
  const [check, setChecked] = useState(true);
  

  useEffect( () => {
  }, []);

  return (
    <div>

      {/*----------Experiment-----------------------Experiment-------------*/}
      <h3>{name.value} - Funciton Component (Hook)</h3>
      <input {...name}/>
      <select {...cars}>
        <option>suzuki</option>
        <option>mercedes</option>
      </select>
      <input type="checkbox" checked={check} onChange={()=>setChecked(!check)}/>
      <pre>{JSON.stringify(name)}</pre>
      <pre>{JSON.stringify(cars)}</pre>
      
    </div>
  )
}

export default Experiment;
import React from "react";
import { NavLink, useLocation } from "react-router-dom";

import Login from "./login";

const Header = (test) => {
    var location = useLocation();
    
    return (
      <nav>
        {/*--Tambah CSS Aktif (Active Style/activeClassName)--*/}
        <div className="navbar" style={{justifyContent: 'space-between'}}>
          <div style={{display: 'flex'}}>
            <NavLink to="/component" activeStyle={{ color: "#476901" }}>Component</NavLink>
            <NavLink to="/parsing-data" activeClassName="active">Parsing Data</NavLink>
            <NavLink to="/form" activeClassName="active">Form</NavLink>
            <NavLink to="/plugins" activeClassName="active">Plugins & Modules</NavLink>
            <NavLink to="/experiment" activeClassName="active">Experiment & Test</NavLink>
          </div>
          <Login/>
        </div>
        <div className="line"></div>

        <div className="navbar small">
          {location.pathname.includes('component') &&
            (<>
              <NavLink to="/component/class" exact activeClassName="active">Class Component</NavLink>
              <NavLink to="/component/hook" exact activeClassName="active">Hooks Component</NavLink>
              <NavLink to="/component/404" exact activeClassName="active">404 Component</NavLink>
              <br/>
            </>)        
          }
          
          {location.pathname.includes('parsing-data') &&
            (<>
              <NavLink to="/parsing-data" exact activeClassName="active">Class Component</NavLink>
              <NavLink to="/parsing-data/hook" exact activeClassName="active">Hooks Component</NavLink>
              <NavLink to="/parsing-data/convertdata" exact activeClassName="active">Formated Data</NavLink>
              <NavLink to="/parsing-data/fetch-api" exact activeClassName="active">API Call</NavLink>
              <NavLink to="/parsing-data/firebase-api" exact activeClassName="active">Firebase Database</NavLink>
              <br/>
            </>)
          }
          
          {location.pathname.includes('form') &&
          (<>
            <NavLink to="/form/class" activeClassName="active">Class Component</NavLink>
            <NavLink to="/form/hook" activeClassName="active">Hooks Component</NavLink>
          </>)
          }

          {location.pathname.includes('plugins') &&
            (<>
              <NavLink to="/plugins/pagination" exact activeClassName="active">Pagination</NavLink>
              <NavLink to="/plugins/slider" exact activeClassName="active">Slider</NavLink>
            </>)        
          }
          {location.pathname.includes('experiment') &&
            <NavLink to="/experiment" activeClassName="active">Experiment & Test</NavLink>
          }
        </div>
        
      </nav>
    );
  
}

export default Header;

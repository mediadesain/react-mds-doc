import React from "react";
import { Redirect } from "react-router-dom";

class FatchAPIDetail extends React.Component {
  constructor(props) {
    super(props); //Location Paramnya console.log(this.props.match.params.detailnya)
    this.state = {};
  }
  
  componentDidMount() {
    //API Call GET
    var url = this.props.match.params.detailnya;
    fetch('https://anomali-st1.firebaseio.com/products.json?orderBy="url"&equalTo="'+ url +'"', {
      method: "GET",
      headers: {"Content-type": "application/json;charset=UTF-8"}
    })
    .then(res => res.json())
    .then(
      (result) => {
        if (Object.keys(result).length === 0) this.setState({redirect: "/404"});
        else {
          var datecreate = new Date(Object.values(result)[0].datecreate).toLocaleString();
          var data = Object.values(result)[0];
          data["datecreate"] = datecreate;
          this.setState({isLoaded: true,data});
          document.title = "Detail - " + data.name;
        }
      },
      (error) => {
        this.setState({isLoaded: true,error});
      }
    )
    .catch(err => console.log(err));
  }

  submit = (val) => {
    //API Call POST
    fetch('https://anomali-st1.firebaseio.com/test.json', {
      method: "POST",
      body: JSON.stringify(val),
      headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(response => response.json()) 
    .then(json => console.log('sucess',json))
    .catch(err => console.log('error',err));
  }

  render() {
    console.log("Final Data", this.state);
    if (this.state.redirect)  return <Redirect exact to={this.state.redirect} />;
    return (
      <>
        {this.state.data ? (
          <div>
            <h3>
            {this.state.data.name} - {this.state.data.brand}
            </h3>
            <h4>{this.state.data.datecreate}</h4>
            <p>{this.state.data.description}</p>
          </div>
        ) : (<h3>Loading</h3>)}
        <button onClick={()=>this.submit({test:'zzz'})}>Submit</button>
      </>
    );
  }
}

export default FatchAPIDetail;

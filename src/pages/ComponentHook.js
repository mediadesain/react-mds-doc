/*=====================================================
----------Contoh Functional Component (Hook)----------
=====================================================*/

import React, { useState, useEffect } from 'react';

const About = (props) => {
  const [judul, setJudul] = useState("Hook Component");

  //useEffect Pengganti componentDidMount() componentDidUpdate() componentWillUnmount()
  useEffect( () => {
    document.title = judul
  })

  const perbaharui = (newval) => {
    console.log('Data Barunya adalah:', newval)
    setJudul(newval)
  }

  return (
    <div>
      <h3>{judul} - Funciton Component (Hook)</h3>
      <button onClick={()=>perbaharui('Hook Component (Updated)')}>Update</button>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam molestie quis
        mi et placerat. Fusce ut tristique libero. Pellentesque dictum velit nunc,
        fringilla rutrum lorem blandit at.
      </p>
      <p>
        Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
        inceptos himenaeos. Donec aliquam sodales eleifend. Praesent tempus porta
        nisl eget eleifend.
      </p>
    </div>
  );
}
export default About;


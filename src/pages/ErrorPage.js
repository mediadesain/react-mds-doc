import React, { useEffect } from 'react';

const ErrorPage = () => {
  useEffect( () => {
    document.title = "404 Page Not Found";
  })
  return (
    <div className="container">
      <h4>Error: page not found</h4>
    </div>
  );
}

export default ErrorPage;

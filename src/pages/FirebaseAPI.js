import React, { useState, useEffect } from "react";
import useProducts from '../services/ProductApi';
import {NavLink} from 'react-router-dom';

const FirebaseAPI = () => {
    const { sourcedata } = useProducts(); //data from firebase
    const [data, setData] = useState();
    
    useEffect( () => {
        document.title = "Fetch Firebase RealtimeDatabase";
        let _isMounted = false;
        //Edit & Formating Data
        if(sourcedata){
            var data = sourcedata.forEach( (val)=>{
                val.variant = JSON.parse(val.variantstring);
            } )
            data = sourcedata.filter(x => x.brand.includes(''));
            console.log(data)
        }
        if(!_isMounted){ setData(data) }
        return () => { _isMounted = true; }
    }, [sourcedata]);

    return (
        <div className="row">
            <div className="column" style={{minWidth:'60%'}}>
                <h3>Demo Parsing Realtime Database Firebase API</h3>
                <small>(Total {data?.length} Products)</small>
                <ul>
                {data ? (
                    data.map((val, i) => (
                        <div key={i}>
                            {val.name} - {val.brand} - {val.sku}
                            <br />
                            <NavLink to={"/parsing-data/fetch-api/" + val.url}>Lihat Detail</NavLink>
                        </div>
                    ))
                ) : (
                    //null
                    <div>
                        <h3>Loading</h3>
                        <p>asd</p>
                    </div>
                )}
                </ul>

                <NavLink to={"/parsing-data/pagination"}>With Pagination</NavLink>
            </div>
            <div className="column" style={{minWidth:'40%'}}>
                <pre>data: {JSON.stringify(data,0,1)}<br/>...</pre>
            </div>
        </div>
    )
}

export default FirebaseAPI;

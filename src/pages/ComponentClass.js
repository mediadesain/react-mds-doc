/*=====================================================
----------------Contoh Class Component----------------
=====================================================*/

import React, { Component } from 'react';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      judul: "Class Components"
    };
  }

  componentDidMount() {
    console.log('Auto mount')
    document.title = this.state.judul;
  }

  componentDidUpdate() {
    console.log('Jika update')
    document.title = this.state.judul;
  }

  componentWillUnmount() {
    console.log('Ganti route')
  }
  
  perbaharui(newval){
    console.log('Data Barunya adalah:', newval)
    this.setState({
      judul: newval
    })
  }

  render() {
    return (
      <div>
        <h3>{this.state.judul} - Class Component</h3>
        <button onClick={()=>this.perbaharui('Class Component (Updated)')}>Update</button>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam molestie quis
          mi et placerat. Fusce ut tristique libero. Pellentesque dictum velit nunc,
          fringilla rutrum lorem blandit at.
        </p>
        <p>
          Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
          inceptos himenaeos. Donec aliquam sodales eleifend. Praesent tempus porta
          nisl eget eleifend.
        </p>
      </div>
    );
  }
}

export default Home;
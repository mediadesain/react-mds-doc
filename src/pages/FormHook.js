import React, { useEffect, useState } from "react";
import { cleanObjectNull } from '../helper';

const FormHook = () => {
  const data = {
      username: "John Doe",
      gender: "male",
      age: 123,
      address: {
        detail: "cipinang",
        zip: "123"
      },
      _error: {}
  };
  const [state, setState] = useState(data);
  const [error, setError] = useState(false);
  
  useEffect( () => {
    document.title = "Form - Function Component";
  }, [state])
  
  const change = (e) => {
    //Identifikasi objek properti dan valuenya
    let name = e.target.name;
    let value = e.target.value;
    let errortxt = null;
    //Identifikasi form username
    if (name === "username") {
      if (value.length <3) errortxt = "Minimal 3 karater";
      if (value === "") errortxt = "Isi Form Ini";
    };
    //Identifikasi form age
    if (name === "age") {
      if (!Number(value)) errortxt = "Pastikan umur adalah angka";
      if (value === "") errortxt = "Isi Form Ini";
    };
    //Memperbaharui state data
    setState({
      ...state,
        [name]: value,
        _error: {
          ...state._error,
          [name]: errortxt
        }
    })
    //Checking Status Error
    var errorlist = {
      ...state._error,
      [name]: errortxt
    }
    var totalerror = Object.keys(cleanObjectNull(errorlist));
    if (totalerror != 0) setError(true)
    else setError(false)
  };

  const changeGroup = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    setState({
      ...state,
        address: {
          ...state.address,
          [name]:value
        }
    })
  };
  
  const submit = (e) => {
    e.preventDefault();
    alert('Value: '+JSON.stringify(state));
    console.log("submit:", state);
  };
  
  return (
    <div className="row">
        <div className="column" style={{minWidth:'60%'}}>
          <form onSubmit={e=>submit(e)}>
            <h4>Form (Hook Version)</h4>
            <p>Nama Lengkap:</p>
            <input type="text" className={state._error.username ? "danger" : null} name="username" value={state.username} onChange={e=>change(e)}/>
            <small className="danger">{state._error.username}</small>
            <p>Umur:</p>
            <input type="text" className={state._error.age ? "danger" : null} name="age" value={state.age} onChange={e=>change(e)} required/>
            <small className="danger">{state._error.age}</small>
            <p>Jenis Kelamin:</p>
            <select name="gender" value={state.gender} onChange={e=>change(e)}>
              <option value="male">Pria</option>
              <option value="female">Wanita</option>
              <option value="others">Lainnya</option>
            </select>
            <p>Alamat Lengkap:</p>
            <input type="text" name="detail" value={state.address.detail} onChange={e=>changeGroup(e)}/>
            <p>Code Pos:</p>
            <input type="text" name="zip" value={state.address.zip} onChange={e=>changeGroup(e)}/>
            <br /><br />
            <button disabled={state._error.username || state._error.age}>Submit</button><br/>
            {error && <small className="danger">Mohon Perbaiki Kesalahan Input</small>}
          </form>
        </div>
        <div className="column">
          <small>
            <pre>error: {JSON.stringify(error)}</pre>
            <pre>data: {JSON.stringify(state,0,2)}</pre>
          </small>
        </div>
      </div>
    );
}

export default FormHook;
/*=====================================================
------------ Contoh Parsing Data from API ------------
=====================================================*/

import React, { Component } from "react";
import { NavLink } from "react-router-dom";
//import {upperCase} from './../helper'
//import qs from "stringquery";

class FatchAPI extends Component {
  _isMounted = false;

  constructor(props) {
    super(props); //Location Paramnya console.log(props)
    this.state = {};
    
    //Query params add url = ?brand=apple&category=smartphone
    //const obj = qs(props.location.search);
    //console.log(obj);
  }

  componentDidMount() {
    document.title = "Fetch API Call";
    this._isMounted = true;

    //API Call
    fetch("https://anomali-st1.firebaseio.com/products.json", {
      method: "GET",
      headers: {"Content-type": "application/json;charset=UTF-8"}
    })
    .then(res => res.json())
    .then(
      (result) => { //console.log("result", result);
        if (this._isMounted) {
          this.setState({
            isLoaded: true,
            data: Object.values(result)
          });
        }
      },
      (error) => { //console.log("error", error);
        if (this._isMounted) {
          this.setState({
            isLoaded: false,
            error
          });
        }
      }
    );
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    console.log("data", this.state);
    return (
      <div className="row">
          <div className="column" style={{minWidth:'60%'}}>
            <h3>Demo Parsing Data from API</h3>
            <small>(Total {this.state.data?.length} Products)</small>
            <ul>
              {this.state.isLoaded ? (
                this.state.data.map((val, i) => (
                  <div key={i}>
                    {val.name} - {val.brand} - {val.sku} - {val.datecreate}
                    <br />
                    <NavLink to={"/parsing-data/fetch-api/" + val.url}>Lihat Detail</NavLink>
                  </div>
                ))
              ) : (
                //null
                <div>
                  <h3>Loading</h3>
                  <p>asd</p>
                </div>
              )}
            </ul>
            <NavLink to={"/parsing-data/pagination"}>With Pagination</NavLink>
          </div>
          <div className="column" style={{minWidth:'40%'}}>
            <pre>data: {JSON.stringify(this.state.data,0,1)}<br/>...</pre>
          </div>
      </div>
    );
  }
}

export default FatchAPI;

import React, { Component } from "react";
import { cleanObjectNull } from '../helper';

class Form extends Component {
  constructor(props){
    super(props);
    this.state = {
      username: "John Doe",
      gender: "male",
      age: 123,
      address: {
        detail: "cipinang",
        zip: "123"
      },
      _error: {}
    };
    this.error = false;
  }
  componentDidMount = () => {
    document.title = "Form - Class Component";
  }
   clean = (obj) => {
    for (var propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined) {
        delete obj[propName];
      }
    }
    return obj
  }
  change = (e) => {
    //Identifikasi objek properti dan valuenya
    let name = e.target.name;
    let value = e.target.value;
    let errortxt = null;
    //Identifikasi form username
    if (name === "username") {
      if (value.length <3) errortxt = "Minimal 3 karater";
      if (value === "") errortxt = "Isi Form Ini";
    };
    //Identifikasi form age
    if (name === "age") {
      if (!Number(value)) errortxt = "Pastikan umur adalah angka"; 
      if (value === "") errortxt = "Isi Form Ini";
    };
    //Memperbaharui state data
    this.setState(state=>({
      ...state,
      [name]: value,
      _error: {
        ...state._error,
        [name]: errortxt
      }
    }));
    //Checking Status Error
    var errorlist = {
      ...this.state._error,
      [name]: errortxt
    }
    var totalerror = Object.keys(cleanObjectNull(errorlist));
    if (totalerror != 0) this.error = true;
    else this.error = false;
  };

  changeGroup = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
        address: {
          ...this.state.address,
          [name]:value
        }
    });
  };
  
  submit = (e) => {
    e.preventDefault();
    alert('Value: '+JSON.stringify(this.state));
    console.log("submit:", this.state);
  };
  
  render() {
    return (
        <div className="row">
          <div className="column" style={{minWidth:'60%'}}>
            <form onSubmit={this.submit}>
              <h4>Form</h4>
              <p>Nama Lengkap:</p>
              <input className={this.state._error.username ? "danger" : null} type="text" name="username" value={this.state.username} onChange={this.change}/>
              <small className="danger">{this.state._error.username}</small>
              <p>Umur:</p>
              <input className={this.state._error.age ? "danger" : null} type="text" name="age" value={this.state.age} onChange={this.change} required/>
              <small className="danger">{this.state._error.age}</small>
              <p>Jenis Kelamin:</p>
              <select name="gender" value={this.state.gender} onChange={this.change}>
                <option value="male">Pria</option>
                <option value="female">Wanita</option>
                <option value="others">Lainnya</option>
              </select>
              <p>Alamat Lengkap:</p>
              <input type="text" name="detail" value={this.state.address.detail} onChange={this.changeGroup}/>
              <p>Code Pos:</p>
              <input type="text" name="zip" value={this.state.address.zip} onChange={this.changeGroup}/>
              <br /><br />
              <button disabled={this.state._error.username || this.state._error.age}>Submit</button><br/>
              {this.error && <small className="danger">Mohon Perbaiki Kesalahan Input</small>}
            </form>
          </div>
          <div className="column">
            <small>
              <pre>error: {JSON.stringify(this.error,0,2)}</pre>
              <pre>data: {JSON.stringify(this.state,0,2)}</pre>
            </small>
          </div>
        </div>
    );
  }
}

export default Form;

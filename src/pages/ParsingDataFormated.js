/*=====================================================
------------- Contoh Parsing Data (HOOK) -------------
=====================================================*/

import React, { useState, useEffect } from 'react';
import {tanggal, uangRupiah, lowerCase, titleCase, pascalCase, uangDollar, upperCase, youtubeID, youtubeEmbed} from '../helper'

const ParsingDataFormated = (props) => {
    const [state] = useState({ brand: "Ford", description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquam scelerisque cursus. Fusce iaculis ornare consectetur. Sed consectetur nisi eget eleifend vehicula.", model: "Mustang", color: "Red", year: 1964, date: 1613026223952, money: 50000 });

    useEffect( () => {
        document.title = "Parsing & Format Data";
    })


  return (
    <div>
      <h3>Formating Data</h3>
      <p>Sum 5 + 5 {5 + 5}</p>
      <table style={{width:'100%'}}>
        <thead>
          <tr>
            <th style={{width:'400px'}}>Original Data</th>
            <th>Fungsi</th>
            <th style={{width:'400px'}}>Formated</th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <td>
          {state.brand + ' ' + state.model + ' ' + state.color}
          </td>
          <td>
            lowerCase()<br/>
            titleCase()<br/>
            upperCase()<br/>
            pascalCase()<br/>
          </td>
          <td>
            {lowerCase(state.brand + ' ' + state.model + ' ' + state.color)}<br/>
            {titleCase(state.brand + ' ' + state.model + ' ' + state.color)}<br/>
            {upperCase(state.brand + ' ' + state.model + ' ' + state.color)}<br/>
            {pascalCase(state.brand + ' ' + state.model + ' ' + state.color)}
          </td>
        </tr>
        <tr>
          <td>{state.date}<br/></td>
          <td>tanggal()</td>
          <td>{tanggal(state.date)}<br/></td>
        </tr>
        <tr>
          <td>{state.money}<br/></td>
          <td>
            uangRupiah()<br/>
            uangDollar()
          </td>
          <td>
            {uangRupiah(state.money)}<br/>
            {uangDollar(state.money)}
          </td>
        </tr>
        <tr>
          <td>https://www.youtube.com/watch?v=xnJb5YgQikM<br/></td>
          <td>
            youtubeID()<br/>
            youtubeEmbed()
          </td>
          <td>
            {youtubeID('https://www.youtube.com/watch?v=xnJb5YgQikM')}<br/>
            {youtubeEmbed('https://www.youtube.com/watch?v=xnJb5YgQikM')}
          </td>
        </tr>
        </tbody>
      </table>
      <p><i>Further documentation moved to <a href="https://bitbucket.org/mediadesain/javascript-mds-doc">https://bitbucket.org/mediadesain/javascript-mds-doc</a></i></p>
    </div>
  );
}
export default ParsingDataFormated;
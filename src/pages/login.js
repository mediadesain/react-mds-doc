import React from 'react';
import firebase from "../firebase";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      errorMessage: "",
      errorCode: "",
      displayLogin: false
    };
  }
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) { //console.log("You are logged in", user);
        this.setState({ user });
      } else {
        //console.log("You are NOT logged in", user);
      }
    });
  }
  submitSignIn = (e) => {
    e.preventDefault();
    firebase.auth().signInWithEmailAndPassword("reynaldi.setiabudi@anomalistudio.com", "b%s6kd78")
      .catch(error => {
        var errorCode = error.code;
        var errorMessage = error.message;
        this.setState({ errorMessage, errorCode });
      });
      console.log(this.state)
  };
  signOut = () => {
    firebase.auth().signOut().then(s => {
        this.setState({ user: null });
      });
  };
  render() {
    //console.log(this.state.user)
    if(this.state.user==null){
      return (
        <div style={{minWidth:'100px'}}>
          <span style={{float:'right'}}>
            <button onClick={this.submitSignIn}>Login</button>
          </span>
          <span style={{float:'right'}}>
            <button>Reg</button>
          </span>
        </div>
      )
    } else {
      return (
        <span style={{float:'right'}}> 
          {' '+this.state.user.email}
          <button onClick={this.signOut}>Sign out</button>
        </span>
      );
    }
    
  }
}

export default Login;
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom"; //Install npm i react-router-dom

import "./styles/slick-carousel/1.6.0/slick.min.css";
import "./styles/slick-carousel/1.6.0/slick-theme.min.css";
import "./style.css";

import Header from "./pages/Header";

import Footer from "./pages/Footer";
import ComponentClass from "./pages/ComponentClass";
import ComponentHook from "./pages/ComponentHook";
import ErrorPage from "./pages/ErrorPage";
import ParsingData from "./pages/ParsingData";
import ParsingDataHook from "./pages/ParsingDataHook";
import ParsingDataFormated from "./pages/ParsingDataFormated";
import FetchAPI from "./pages/FatchAPI";
import FetchAPIDetail from "./pages/FatchAPIDetail";
import FirebaseAPI from "./pages/FirebaseAPI";
import Form from "./pages/Form";
import FormHook from "./pages/FormHook";
import PluginPagination from "./pages/PluginPagination";
import SlickJs from "./pages/PluginSlickJs";
import Experiment from "./pages/Experiment";

class App extends Component {
  render() {
    return (
      <Router>
          <Header />
          <main className="container">
            <Switch>
              <Redirect from="/" to="/component/class" exact></Redirect>
              <Redirect from="/component" to="/component/class" exact></Redirect>
              <Route path={"/component/class"} component={ComponentClass}/>
              <Route path={"/component/hook"} component={ComponentHook}/>
              <Route path={"/parsing-data"} component={ParsingData} exact/>
              <Route path={"/parsing-data/hook"} component={ParsingDataHook} />
              <Route path={"/parsing-data/convertdata"} component={ParsingDataFormated} />
              <Route path={"/parsing-data/fetch-api"} component={FetchAPI} exact/>
              <Route path={"/parsing-data/fetch-api/:detailnya"} component={FetchAPIDetail} />
              <Route path={"/parsing-data/firebase-api"} component={FirebaseAPI} exact/>
              <Redirect from="/form" to="/form/class" exact></Redirect>
              <Route path={"/form/class"} component={Form} />
              <Route path={"/form/hook"} component={FormHook} />
              <Route path={"/experiment"} component={Experiment} />
              <Redirect from="/plugins" to="/plugins/pagination" exact/>
              <Route path={"/plugins/pagination"} component={PluginPagination} />
              <Route path={"/plugins/slider"} component={SlickJs} />
              <Route component={ErrorPage} />
            </Switch>
          </main>
          <Footer />
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));

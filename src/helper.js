//import React, {useEffect, useState} from 'react'; (Alternative)
import React from 'react';

//Judul AddressBar dibrowser
export const useJudul = (title) => {
    React.useEffect( () => {document.title = title})
};
  
//Update isian Form 
export const usePerbaharuiForm = (newVal) => {
    const [value, setValue] = React.useState(newVal);
    function changeValue(e){ setValue(e.target.value)}
    return {
        value,
        onChange: changeValue
    };
};

//=======================Custom Function=======================//
export const tanggal = (date) => {
    const formatDate = new Date(date);
    const tdy = formatDate.getDay()
    const dayName = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ][tdy]
    const d = formatDate.getDate();
    const m = formatDate.getMonth();
    const monthName =  [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ][m]
    const y = formatDate.getFullYear();
    date = dayName+', '+d+' '+monthName+' '+y; //console.log(tdy, dayName, d, monthName, y)
    return date
};

//----------Konfersi String----------//
export const lowerCase = (str) => {
    return str.toLowerCase();
};
export const upperCase = (str) => {
    return str.toUpperCase();
};
export const titleCase = (str) => {
    return str.replace(/\b(\w)/g, k => k.toUpperCase());
};
export const pascalCase = (str) => {
    return str.toLowerCase().replace(/[^\w\s]/gi, ' ').replace(/(?:_| |\b)(\w)/g, function (str, p1) { return p1.toUpperCase() }).replace(/\s/g, '');
};
export const putstrMds = (a) => {
    return [a.slice(0, 3),'md',a.slice(3),'ds'].join('');
}
export const randomChar = (length) => {
    var chars = 'abcefghijklnopqrtuvwxyz0123456789'; //sample random chars
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
};
export const slugifyURL = (text) => {
    if (!text) return;
        var result = text.toLowerCase().replace('.', ' ').replace('-', ' ').replace(/[^\w ]+/g, '').replace(/ +/g, '-');
    return result;
};

//----------Konfersi Object----------//
//Hapus Object Properti jika valuenya null / undefined
export const cleanObjectNull = (obj) => {
    for (var namaProperty in obj) {
        if (obj[namaProperty] === null || obj[namaProperty] === undefined) {
            delete obj[namaProperty];
        }
    } return obj
};

//----------Mata Uang----------//
export const uangRupiah = (money) => {
   return new Intl.NumberFormat('id-ID',
     { style: 'currency', currency: 'IDR', minimumFractionDigits: 2 }
   ).format(money);
}
export const uangDollar = (money) => {
   return new Intl.NumberFormat('en-US',
     { style: 'currency', currency: 'USD', minimumFractionDigits: 2 }
   ).format(money);
}

//----------Youtube----------//
export const youtubeID = (url) => {
    var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    var match = url.match(regExp);
    if (match && match[2].length === 11) return match[2];
    else return '';
};
export const youtubeEmbed = (val) =>{
    val = youtubeID(val)
    return 'https://www.youtube.com/embed/'+ val;
}

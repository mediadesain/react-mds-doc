import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

const app = firebase.initializeApp({
  apiKey: "AIzaSyAjpuUHbSGuCX1NEDkE0yIXhiP4kmjcCxk",
  authDomain: "anomali-st1.firebaseapp.com",
  databaseURL: "https://anomali-st1.firebaseio.com",
  projectId: "anomali-st1",
  storageBucket: "anomali-st1.appspot.com",
  messagingSenderId: "910994146715",
  appId: "1:910994146715:web:0a57db27f802db30868d3e",
  measurementId: "G-08LMCHQHHZ"
});

export default app;